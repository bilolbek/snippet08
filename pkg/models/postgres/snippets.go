package postgres

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"se08.com/pkg/models"
	"strconv"
	"time"
)

type SnippetModel struct {
	Pool *pgxpool.Pool
}

const format = "2006-01-02 15:04:05"

var tm = time.Now().Format(format)

// This will insert a new snippet into the database.
func (m *SnippetModel) Insert(title, content, expires string) (int, error) {

	stmt := `INSERT INTO snippets (title, content, created, expires) VALUES($1, $2, $3, $4) RETURNING id`

	dt := time.Now()
	dt2, _ := strconv.Atoi(expires)
	exp := dt.AddDate(0, 0, dt2)
	createdt := dt.Format(format)
	expiredt := exp.Format(format)

	var id int
	err := m.Pool.QueryRow(context.Background(), stmt, title, content, createdt, expiredt).Scan(&id)
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

// This will return a specific snippet based on its id.
func (m *SnippetModel) Get(id int) (*models.Snippet, error) {

	stmt := `SELECT id, title, content, created, expires FROM snippets
WHERE expires > $1 AND id = $2`

	row := m.Pool.QueryRow(context.Background(), stmt, tm, id)

	s := &models.Snippet{}

	err := row.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {

		if errors.Is(err, pgx.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}

	return s, nil

}

// This will return the 10 most recently created snippets.
func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	stmt := `SELECT id, title, content, created, expires FROM snippets
WHERE expires > $1 ORDER BY created DESC LIMIT 10`

	rows, err := m.Pool.Query(context.Background(), stmt, tm)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	snippets := []*models.Snippet{}

	for rows.Next() {

		s := &models.Snippet{}

		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}
		snippets = append(snippets, s)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return snippets, nil

}
