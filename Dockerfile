# Build executable stage
FROM golang
LABEL maintainer="bilolbek00@gmail.com"
# копирует все файлы (.)c директории в котором находися докерфайл в контейнер ( папку app (/app))
ADD . /app
# устанавливаем рабочую директрию контейнера
# С этой директорией работают инструкции COPY, ADD, RUN, CMD и ENTRYPOINT, идущие за WORKDIR
WORKDIR /app/cmd/web
# собираем/создам слой
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build # This will create a binary file named app
# указываем файл на бинарник с названием web (бианарник берет название рабочей директории)
ENTRYPOINT /app/cmd/web/web


# Build final image
FROM alpine:latest
#(менеджер пакетов Alpine Linux) .
RUN apk --no-cache add ca-certificates
WORKDIR /root/
# копируем
COPY --from=0 /app/cmd/web/web .
# описываем команду с аргументами, которую нужно выполнить когда контейнер будет запущен.
CMD ["./web"]

